# Use docker image "maven" with Maven v3.x and JDK 11
image: maven:3-jdk-11

variables:
    # MAVEN_OPTS are command line parameters passed to the JVM that executes Maven
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

  # Directories to cache and key to identify cache
cache:
  key: mycachekey
  paths:
    - .m2/repository
      
# Define some compile job to be used as build stage (calls Maven)
mycompile:
  stage: build
  # Execute command "mvn compile -–batch mode"
  script:
    - mvn compile --batch-mode
  # We want to keep the files of the build stage so that it can be re-used by test stage
  artifacts:
    paths:
      - "src"
      - "target"
      - "pom.xml"
    expire_in: 1h

# Define some unit test job to be used as (unit) test stage (calls Maven)
myunittest:
  # Gitlab runs stage test after stage build
  stage: test
  # Execute command "mvn test -–batch mode"
  variables:
    #prevent git clone (we import files as artefacts from previous state)
    GIT_STRATEGY: none
  script:
    - mvn test --batch-mode
    # Print JaCoCo result to console log of runner (will be parsed by GitLab)
    - cat target/site/jacoco/index.html | grep -o '<tfoot>.*</tfoot>'
  # Let GitLab parse code coverage from index.html and display it in web interface
  coverage: '/Total.*?([0-9]{1,3})%/'
  dependencies:
    - mycompile
  artifacts:
    paths:
      - "target"
      - "pom.xml"
    expire_in: 1h

# Important: the name of this job must be "pages":
pages:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - mvn site --batch-mode
    - mkdir public
    - mv target/site/* public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  dependencies:
    - myunittest
  only:
    - master

sonarcloud-check:
  stage: test
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar
  only:
    - merge_requests
    - master
    - develop